#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "LCD/HD44780.h"
#include <stdlib.h>

#define KEY_DOWN !(PINA & (1<<PA1)) 

void trigger(void); // wyzwolenie czujnika HC SR04

char temp2[7];
char temp_min[3];

volatile uint16_t pulseTime; 	// d�ugo�� czasu do powrotu sygna�u
uint16_t temp_dist;
uint8_t min_dist = 15;		// minimalny dystans od przeszkody
uint8_t pomiar_licznik;
uint16_t usredniony_pomiar;
uint16_t distance;

int main() {

	DDRC |= (1 << PC0);   //wyjscie
	PORTC &= ~(1 << PC0); //TRIGGER

	DDRD &= ~(1 << PD6);  //wejscie ECHO
	PORTD &= ~(1 << PD6); //ICP

	DDRA &= ~(1 << PA1); 	// wejscie
	PORTA |= (1 << PA1); 	// uSWITCH

	LCD_Initalize();

// Inicjalizacja Timera
	TCCR1B |= (1 << ICES1);
	TCCR1B |= (1 << CS11);
	TCCR1B |= (1 << ICNC1);
	TIMSK |= (1 << TICIE1);

	sei();

	while (1) {
		DDRA &= ~(1 << PA0); //wygaszenie LED

		trigger(); //wyzwolenie

		temp_dist += pulseTime; //zbieranie wynik�w do usrednienia
		pomiar_licznik++;
		if (pomiar_licznik == 10) {
			usredniony_pomiar = temp_dist / pomiar_licznik; 
			temp_dist = 0;
			pomiar_licznik = 0;

// Wy�wietlenie u�rednionego wyniku pomiaru na wy�wietlaczu

			LCD_GoTo(0, 0);
			LCD_WriteText("Distance: ");
			distance = usredniony_pomiar / 58;
			if (distance > 100) {
				LCD_GoTo(0, 0);
				LCD_WriteText("  [OVERRANGE]   ");
			} else {
				itoa(distance, temp2, 10);
				LCD_WriteText(temp2);
				LCD_WriteText(" cm    ");
			}

// Alarm w przypadku przekroczenia minimalnego dystansu

			if (distance < min_dist) {
				DDRA |= (1 << PA0);
				LCD_GoTo(0, 1);
				LCD_WriteText("   [ALARM!!!]   ");
			}

// Obs�uga klawisza

			if (KEY_DOWN) {
				_delay_ms(80);
				min_dist++;
				if (min_dist > 50)
					min_dist = 0;
			}

			itoa(min_dist, temp_min, 10);
			LCD_GoTo(0, 1);
			LCD_WriteText("Min.: ");
			LCD_WriteText(temp_min);
			LCD_WriteText(" cm    ");
		}

	}

}

ISR(TIMER1_CAPT_vect)
{
	TCNT1 = 0;
	if( !(TCCR1B & (1<<ICES1)) ) pulseTime = ICR1;

	TCCR1B ^= (1<<ICES1);
}

void trigger(void) {
	PORTC &= ~(1 << PC0);
	_delay_us(2);
	PORTC |= (1 << PC0); 	//wystawienie jedynki na TRIG
	_delay_us(10);        	//wymagane 10 uS stanu wysokiego
	PORTC &= ~(1 << PC0);   //wystawienie zera na TRIG
}
